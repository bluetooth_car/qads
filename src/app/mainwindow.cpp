#include "mainwindow.h"

#include <QSettings>
#include <QDebug>

#include "advanceddockingsystem/dockmanager.h"
#include "advanceddockingsystem/dockareawidget.h"
#include "advanceddockingsystem/dockwidget.h"

using namespace ADS;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QSettings *settings = new QSettings(tr("qads"));

    DockManager *manager = new DockManager(this);
    manager->setSettings(settings);

    QFile file(":/main/dockwidgets");
    if (file.open(QFile::ReadOnly)) {
        QString qss = QLatin1String(file.readAll());
        manager->setStyleSheet(qss);
        file.close();
    }

    DockWidget *dw = new DockWidget("test1");
    dw->setWidget(new QWidget());
    manager->addDockWidget(LeftDockWidgetArea, dw);
    dw = new DockWidget("test1");
    dw->setWidget(new QWidget());
    manager->addDockWidget(RightDockWidgetArea, dw);
    dw = new DockWidget("test1");
    dw->setWidget(new QWidget());
    manager->addDockWidget(TopDockWidgetArea, dw);
    dw = new DockWidget("test1");
    dw->setWidget(new QWidget());
    manager->addDockWidget(BottomDockWidgetArea, dw);
    dw = new DockWidget("test1");
    dw->setWidget(new QWidget());
    manager->addDockWidget(CenterDockWidgetArea, dw);

    setCentralWidget(manager);

    resize(800, 600);
}

MainWindow::~MainWindow()
{

}
